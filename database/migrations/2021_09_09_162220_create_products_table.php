<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            if (Schema::hasTable('categories')) {
                $table->foreignId('category_id')->constrained();
            }
            $table->string('name');
            $table->text('description')->nullable();
            $table->boolean('is_new')->default(0);
            $table->boolean('is_featured')->default(0);
            $table->double('price');
            $table->double('cost');
            $table->integer('size')->default(0);
            $table->integer('weight')->default(0);
            $table->integer('quantity')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
