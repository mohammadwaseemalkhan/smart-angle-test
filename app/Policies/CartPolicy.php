<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Cart;
use App\policies\BasePolicy;
use Illuminate\Auth\Access\HandlesAuthorization;

class CartPolicy extends BasePolicy
{
    use HandlesAuthorization;

    private $tableName;
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(Cart $model)
    {
        $this->tableName = $model->getTable();
    }


    /**
     * Determine if the given user can browse the model.
     *
     * @param \App\Models\User $user
     * 
     * @return bool
     */
    public function index(User $user)
    {
        return $user->hasPermission('index_'.$this->tableName);
    }

    /**
     * Determine if the given model can be viewed by the user.
     *
     * @param \App\Models\User $user
     * @param  $model
     *
     * @return bool
     */
    public function read(User $user)
    {
        return $user->hasPermission('read_'.$this->tableName);
    }

    /**
     * Determine if the given model can be updated by the user.
     *
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function update(User $user)
    {
        return $user->hasPermission('update_'.$this->tableName);
    }

    /**
     * Determine if the given user can create the model.
     *
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasPermission('create_'.$this->tableName);
    }
}
