<?php

namespace App\Http\Controllers\API\Products;

use App\Models\Product;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Http\Requests\ProductRequest;

use App\Helpers\{
    JsonResponse,
    ResponseStatus,
    HelperFunction,
};

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $this->authorize('index', Product::class);
            $products = ProductResource::collection(Product::get());
            return JsonResponse::respondSuccess($products, HelperFunction::myTrans('SUCCESS_RESPONSE'), ResponseStatus::OK_SUCCESS);
        } catch (\Exception $e) {
            return JsonResponse::respondError($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        try {
            $this->authorize('create', Product::class);
            $data = $request->all();
            $product = Product::create($data);
            return JsonResponse::respondSuccess($product, HelperFunction::myTrans('SAVE_SUCCESS'), ResponseStatus::CREATED_SUCCESS);
        } catch (\Exception $e) {
            return JsonResponse::respondError($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        try {
            $this->authorize('read', Product::class);
            $data = Product::find($product->id);
            return JsonResponse::respondSuccess($data, HelperFunction::myTrans('SUCCESS_RESPONSE'), ResponseStatus::OK_SUCCESS);
        } catch (\Exception $e) {
            return JsonResponse::respondError($e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        try {
            $this->authorize('update', Product::class);
            $dataProduct = Product::find($product->id);
            $data = $request->all();
            $dataProduct->update($data);
            return JsonResponse::respondSuccess($dataProduct, HelperFunction::myTrans('UPDATE_SUCCESS'), ResponseStatus::OK_SUCCESS);
        } catch (\Exception $e) {
            return JsonResponse::respondError($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        try {
            $this->authorize('delete', Product::class);
            $product->delete();
            return JsonResponse::respondSuccess(null, HelperFunction::myTrans('DELETE_SUCCESS'), ResponseStatus::OK_SUCCESS);
        } catch (\Exception $e) {
            return JsonResponse::respondError(HelperFunction::myTrans('failed_delete_product'));
        }
    }
}
