<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;

use App\Models\User;
use App\Models\Customer;
use App\Models\Seller;

use App\Helpers\{
    JsonResponse,
    ResponseStatus,
    HelperFunction,
};

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        try {
            $data = $request->all();
            $user = User::findByUsername($data['username'])->first();
            if (!empty($user)){
                if (\Hash::check($data['password'], $user->password)) {
                
                    $token = $user->createToken('');
                    $user->save();
                    $result = [
                        'data' => $user,
                        'token' => $token->accessToken,
                    ];
                    return JsonResponse::respondSuccess($result, HelperFunction::myTrans('SUCCESS_RESPONSE'), 200);
                } 
            }
            return JsonResponse::respondError(HelperFunction::myTrans('UNAUTHORISED_LOGIN'), 401);
        } catch (\Exception $e) {
            return JsonResponse::respondError($e->getMessage());
        }
    }

    public function register(RegisterRequest $request) 
    {
        try {
            $data = $request->all();
            
            $final = DB::transaction(function () use($data){
                $data['name'] = $data['first_name'];
                if ($data['type'] == 'customer') {
                    $data['role_id'] = 2;
                    $user = User::create($data);
                    if (!empty($user)) {
                        $data['user_id'] = $user->id;
                        $customer = Customer::create($data);
                        return $customer;
                    }
                } else if ($data['type'] == 'seller') {
                    $data['role_id'] = 3;
                    $user = User::create($data);
                    if (!empty($user)) {
                        $data['user_id'] = $user->id;
                        $seller = Seller::create($data);
                        return $seller;
                    }
                }
            });
            return JsonResponse::respondSuccess($final, HelperFunction::myTrans('SAVE_SUCCESS'), 200);
        } catch (\Exception $e) {
            return JsonResponse::respondError($e->getMessage());
        }
    }
}
