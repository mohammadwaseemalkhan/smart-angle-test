<?php

namespace App\Http\Controllers\API\Carts;

use App\Models\Cart;
use App\Models\CartProduct;
use App\Models\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\CartRequest;

use App\Helpers\{
    JsonResponse,
    ResponseStatus,
    HelperFunction,
};

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Auth, DB};

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $this->authorize('index', Cart::class);
            $cart = Auth::user()->customer->cart;
            return JsonResponse::respondSuccess($cart, HelperFunction::myTrans('SUCCESS_RESPONSE'), ResponseStatus::OK_SUCCESS);
        } catch (\Exception $e) {
            return JsonResponse::respondError($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CartRequest $request)
    {
        try {
            $this->authorize('create', Cart::class);
            $data = $request->all();
            $final = DB::transaction(function() use($data) {
                $cart = Cart::where('customer_id', Auth::user()->customer->id)->first();
                if (!isset($cart)) {
                    $cart = Cart::create([
                        'customer_id' => Auth::user()->customer->id
                    ]);
                } 
                
                $itemsCount = 0;
                $totalPrice = 0;
                foreach ($data['products'] as $key => $product) {
                    $cartProduct = CartProduct::create([
                        'product_id' => $product['product_id'],
                        'cart_id'    => $cart->id,
                        'quantity'   => $product['quantity']
                    ]);
                    $p = Product::find($product['product_id']);
                    $totalPrice += ($p->price * $product['quantity']);
                    $p->update(['quantity' => ($p->quantity - $product['quantity']) ]);
                    $itemsCount += $product['quantity'];
                }

                $cart->update([
                    'items_count' => ($cart->items_count + $itemsCount),
                    'total_price' => ($cart->total_price + $totalPrice),
                ]);
                
                return $cart;
            });
            return JsonResponse::respondSuccess($final, HelperFunction::myTrans('SAVE_SUCCESS'), ResponseStatus::CREATED_SUCCESS);
        } catch (\Exception $e) {
            return JsonResponse::respondError($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show(Cart $cart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(CartRequest $request, Cart $cart)
    {
        try {
            $this->authorize('update', Cart::class);
            if (Auth::user()->customer->cart->id != $cart->id) {
                return JsonResponse::respondError(HelperFunction::myTrans('msg_not_authorized'),ResponseStatus::NOT_AUTHORIZED);
            }

            $deleteProducts = DB::transaction(function() use($cart) {
                $cartProdcut = CartProduct::where('cart_id', $cart->id)->get();
                foreach ($cartProdcut as $key => $value) {
                    $product = Product::find($value['product_id']);
                    $product->update(['quantity' => ($product->quantity + $value['quantity']) ]);
                }
                $cart->update(['items_count' => 0, 'total_price' => 0]);
                return $cartProdcut;
            });
            $deleteProducts = CartProduct::where('cart_id', $cart->id)->delete();
        
            $data = $request->all();
            $final = DB::transaction(function() use($data, $cart) {    
                $itemsCount = 0;
                $totalPrice = 0;
                foreach ($data['products'] as $key => $product) {
                    $cartProduct = CartProduct::create([
                        'product_id' => $product['product_id'],
                        'cart_id'    => $cart->id,
                        'quantity'   => $product['quantity']
                    ]);
                    $p = Product::find($product['product_id']);
                    $totalPrice += ($p->price * $product['quantity']);
                    $p->update(['quantity' => ($p->quantity - $product['quantity']) ]);
                    $itemsCount += $product['quantity'];
                }

                $cart->update([
                    'items_count' => ($cart->items_count + $itemsCount),
                    'total_price' => ($cart->total_price + $totalPrice),
                ]);
                
                return $cart;
            });
            return JsonResponse::respondSuccess($final, HelperFunction::myTrans('SAVE_SUCCESS'), ResponseStatus::OK_SUCCESS);
        } catch (\Exception $e) {
            return JsonResponse::respondError($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cart $cart)
    {
        try {
            $this->authorize('update', Cart::class);
            if (Auth::user()->customer->cart->id != $cart->id) {
                return JsonResponse::respondError(HelperFunction::myTrans('msg_not_authorized'),ResponseStatus::NOT_AUTHORIZED);
            }

            $final = DB::transaction(function() use($cart) {
                $cartProdcut = CartProduct::where('cart_id', $cart->id)->get();
                foreach ($cartProdcut as $key => $value) {
                    $product = Product::find($value['product_id']);
                    $product->update(['quantity' => ($product->quantity + $value['quantity']) ]);
                }
                $cart->update(['items_count' => 0, 'total_price' => 0]);
                return $cartProdcut;
            });
            $deleteProducts = CartProduct::where('cart_id', $cart->id)->delete();
            $cart->delete();
            return JsonResponse::respondSuccess(null, HelperFunction::myTrans('SAVE_SUCCESS'), ResponseStatus::OK_SUCCESS);
        } catch (\Exception $e) {
            return JsonResponse::respondError($e->getMessage());
        }
    }
}
