<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'          => "$this->rule|string|max:255",
            'category_id'   => "$this->rule|numeric|exists:categories,id",
            'is_new'        => "$this->rule|numeric|min:0|max:1",
            'is_featured'   => "$this->rule|numeric|min:0|max:1",
            'price'         => "$this->rule|numeric|min:0",
            'cost'          => "$this->rule|numeric|min:0",
            'quantity'      => "$this->rule|numeric|min:0",
            'description'   => "string",
            'size'          => "numeric|min:0",
            'weight'        => "numeric|min:0",
        ];

        if ($this->isUpdatedRequest()) {
            $rules = [
                'name'          => "string|max:255",
                'category_id'   => "numeric|exists:categories,id",
                'is_new'        => "numeric|min:0|max:1",
                'is_featured'   => "numeric|min:0|max:1",
                'price'         => "numeric|min:0",
                'cost'          => "numeric|min:0",
                'quantity'      => "numeric|min:0",
                'description'   => "string",
                'size'          => "numeric|min:0",
                'weight'        => "numeric|min:0",
            ];
        }

        return $rules;
    }
}
