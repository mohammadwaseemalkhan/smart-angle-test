<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Models\Product;
use Illuminate\Validation\Rule;

class CartRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'products'              => "$this->rule|array",
            'products.*.product_id' => "$this->rule|numeric|exists:products,id",
            'products.*.quantity'   => "$this->rule|numeric|min:1",
        ];

        if (isset(request()->products)) {
            foreach (request()->products as $key => $value) {
                $product = Product::find($value['product_id']);
                $rules['products.'.$key.'.quantity'] = "$this->rule|numeric|min:1|max:$product->quantity";
            }
        }
        

        return $rules;
    }
}
