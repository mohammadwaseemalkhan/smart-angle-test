<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'first_name' => "$this->rule|string|max:255",
            'last_name'  => "$this->rule|string|max:255",
            'email'      => "$this->rule|email:rfc,dns|max:255|unique:users,email",
            'password'   => "$this->rule|string|min:6|max:255",
            'username'   => "$this->rule|string|unique:users,username",
            'birthday'   => "$this->role|date",
            'type'       => [
                "required",
                Rule::in(['customer', 'seller']),
            ],
        ];

        if (isset(request()->type)) {
            if (request()->type == 'customer') {
                $rules['phone'] = "$this->rule|numeric|regex:/^09[0-9]{8}/|unique:customers,phone";
            } else {
                $rules['phone'] = "$this->rule|numeric|regex:/^09[0-9]{8}/|unique:sellers,phone";
            }
        }
        return $rules;
    }
}
