<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'description'   => $this->description,
            'is_new'        => $this->is_new,
            'is_featured'   => $this->is_featured,
            'price'         => $this->price,
            'cost'          => $this->cost,
            'size'          => $this->size,
            'weight'        => $this->weight,
            'quantity'      => $this->quantity,
            'category'      => $this->category,

        ];
    }
}
