<?php

namespace App\Traits;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

use App\Models\Role;


trait UserTrait
{
    /**
     * Return default User Role.
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * Check if User has a Role associated.
     *
     * @param string|array $name The role to check.
     * @author Waseem Al-khen
     * @return bool
     */
    public function hasRole($name)
    {
        $role = $this->role;

        if ($role->name == $name) {
            return true;
        }

        return false;
    }

    public function hasPermission($name)
    {
        $this->loadPermissionsRelations();


        $_permissions = $this->role->permissions
                            ->pluck('key')->unique()->toArray();

        return in_array($name, $_permissions);
    }


    private function loadRolesRelations()
    {
        if (!$this->relationLoaded('role')) {
            $this->load('role');
        }
    }

    private function loadPermissionsRelations()
    {
        $this->loadRolesRelations();

        if ($this->role && !$this->role->relationLoaded('permissions')) {
            $this->role->load('permissions');
        }
    }
}
