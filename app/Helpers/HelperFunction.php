<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;

class HelperFunction 
{
    /**
     * @param string $message
     * @return array|\Illuminate\Contracts\Translation\Translator|string|null
     * @author Waseem Al-khen
     */
    public static function myTrans($message)
    {
        return __('message.' . $message) ?? $message;
    }
}