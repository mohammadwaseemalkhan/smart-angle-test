<?php


namespace App\Helpers;


class JsonResponse
{
    const MSG_ADDED_SUCCESSFULLY = 'message.msg_added_successfully';
    const MSG_UPDATED_SUCCESSFULLY = "message.msg_updated_successfully";
    const MSG_DELETED_SUCCESSFULLY = "message.msg_deleted_successfully";
    const MSG_NOT_ALLOWED = "message.msg_not_allowed";
    const MSG_NOT_AUTHORIZED = "message.msg_not_authorized";
    const MSG_NOT_AUTHENTICATED = "message.msg_not_authenticated";
    const MSG_USER_NOT_ENABLED = "message.msg_user_not_enabled";
    const MSG_NOT_FOUND = "message.msg_not_found";
    const MSG_USER_NOT_FOUND = "message.msg_user_not_found";
    const MSG_EMAIL_NOT_VERIFIED = "your email not verified";
    const MSG_WRONG_PASSWORD = "message.msg_wrong_password";
    const MSG_SUCCESS = "message.msg_success";
    const MSG_FAILED = "message.msg_failed";
    const MSG_LOGIN_SUCCESSFULLY = "message.msg_login_successfully";
    const MSG_LOGIN_FAILED = "message.msg_login_failed";
    const MSG_LOGOUT_SUCCESSFULLY = "message.msg_logout_successfully";

    /**
     * @param null $content
     * @return \Illuminate\Http\JsonResponse
     * @author Waseem Al-khen
     */
    public static function respondSuccess($content = null, $message, $status = 200)
    {
        return response()->json([
            'result' => trans(self::MSG_SUCCESS),
            'content' => $content,
            'message' => $message,
            'status' => $status
        ], $status);
    }

    /**
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     * @author Waseem Al-khen
     */
    public static function respondError($message, $status = 500)
    {
        return response()->json([
            'result' => trans(self::MSG_FAILED),
            'content' => null,
            'message' => $message,
            'status' => $status
        ], $status);
    }
}