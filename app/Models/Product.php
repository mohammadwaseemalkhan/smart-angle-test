<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'category_id',
        'is_new',
        'is_featured',
        'price',
        'cost',
        'quantity',
        'description',
        'size',
        'weight',
    ];

    /**
     * @return array
     * @author Waseem Al-khen
     */
    public function toArray()
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'description'   => $this->description,
            'is_new'        => $this->is_new,
            'is_featured'   => $this->is_featured,
            'price'         => $this->price,
            'cost'          => $this->cost,
            'size'          => $this->size,
            'weight'        => $this->weight,
            'quantity'      => $this->quantity,
            'category'      => $this->category,
        ];
    }

    public function carts()
    {
        return $this->belongsToMany(Carts::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
