<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartProduct extends Model
{
    use HasFactory;

    protected $table = 'cart_product';

    protected $fillable = [
        'product_id',
        'cart_id',
        'quantity',
    ];

   /**
     * @return array
     * @author Waseem Al-khen
     */
    public function toArray()
    {
        return [
            'id'       => $this->id,
            'quantity' => $this->quantity,
            'products' => $this->products,
        ];
    }


    public function carts()
    {
        return $this->belongsTo(Cart::class);
    }

    public function products()
    {
        return $this->belongsTo(Product::class);
    }
}
