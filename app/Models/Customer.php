<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'birthday',
        'phone',
        'user_id',
    ];

    /**
     * @return array
     * @author Waseem Al-khen
     */
    public function toArray()
    {
        return [
            'id'         => $this->id,
            'first_name' => $this->first_name,
            'last_name'  => $this->last_name,
            'birthday'   => $this->birthday,
            'phone'      => $this->phone,
        ];
    }

    public function cart()
    {
        return $this->hasOne(Cart::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
