<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $fillable = [
        'customer_id',
        'items_count',
        'total_price',
    ];

    /**
     * @return array
     * @author Waseem Al-khen
     */
    public function toArray()
    {
        return [
            'id'          => $this->id,
            'items_count' => $this->items_count,
            'total_price' => $this->total_price,
            'customer'    => $this->customer,
        ];
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

}
