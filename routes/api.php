<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\Products\ProductController;
use App\Http\Controllers\API\Carts\CartController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', [AuthController::class, 'login'])->name('phoneLogin');
Route::post('register', [AuthController::class, 'register'])->name('register');

Route::middleware(['auth:api'])->group(function () {
    Route::resource('products', ProductController::class);
    Route::resource('carts', CartController::class);
});
